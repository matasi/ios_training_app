//
//  IncidentDetailController.swift
//  incidents
//
//  Created by Tim Hopkins on 2015-07-31.
//  Copyright (c) 2015 Deloitte Canada. All rights reserved.
//

import Foundation

class IncidentDetailController: UITableViewController {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var incident:Incident?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}