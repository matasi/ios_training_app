//
//  RootViewController.swift
//  incident_mobile
//
//  List view for all incidents
//
//  Created by Tim Hopkins on 2015-07-13.
//  Copyright (c) 2015 Deloitte Canada. All rights reserved.
//

import UIKit
import MapKit


class RootViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, MKMapViewDelegate ,SFRestDelegate {
    
    var incidentList = [Incident]()
    var selectedIncident:Incident?
    
    enum CurrentViewType {
        case Map
        case Table
    }

    var currentView:CurrentViewType? = nil
    var activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(0,0, 50, 50)) as UIActivityIndicatorView
    
    @IBOutlet weak var navgationControl: UINavigationItem!
    @IBOutlet weak var mapViewControl: MKMapView!
    @IBOutlet weak var changeViewSegmentControl: UISegmentedControl!
    @IBOutlet weak var tableViewControl: UITableView!

    @IBAction func segmentControlToggled(sender: AnyObject) {
        if (changeViewSegmentControl.selectedSegmentIndex == 0) {
            setCurrentView(CurrentViewType.Map)
        }
        else {
            setCurrentView(CurrentViewType.Table)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        // add the refresh button to the navigation bar
        var refreshButtonItem:UIBarButtonItem = UIBarButtonItem(title: "Refresh", style: .Plain, target: self, action: "loadIncidentsFromSalesforce")

        navgationControl.leftBarButtonItem = refreshButtonItem
        
        // setup the data source and delegate for the tableView
        tableViewControl.dataSource = self
        tableViewControl.delegate = self
        tableViewControl.registerClass(UITableViewCell.classForCoder(), forCellReuseIdentifier: "identifier")
        
        // default the current view to the Map
        setCurrentView(CurrentViewType.Map)
        
        // add the delegate to the map
        mapViewControl.delegate = self
        
        
        // load the incidents from SFDC
        loadIncidentsFromSalesforce()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // loads the records from SFDC
    func loadIncidentsFromSalesforce() {
        // block input
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        // put the spinner up
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        // load the current incidents from SFDC
        var request = SFRestAPI.sharedInstance().requestForQuery("SELECT id, caseNumber, closedDate, description, isClosed, Rails_id__c, status, subject, accountID, Account.Rails_id__c, Account.Name, CreatedDate,LastModifiedDate, location__latitude__s, location__longitude__s FROM Case WHERE location__latitude__s !=null AND Account.Name != null ORDER BY CreatedDate DESC LIMIT 50")
        SFRestAPI.sharedInstance().send(request,  delegate: self)
    }
    
    // callback post SFDC update
    func updateIncidentsCallback(updatedIncidents:[Incident]) {
        dispatch_async(dispatch_get_main_queue(), {
            // clear all the current entries from the map
            self.mapViewControl.removeAnnotations(self.incidentList)
            // remove the current incident list with the updated version from SFDC
            self.incidentList = updatedIncidents
            // update the table and the map
            self.tableViewControl.reloadData()
            self.mapViewControl.addAnnotations(self.incidentList)
            // allow events again
            self.activityIndicator.stopAnimating()
            UIApplication.sharedApplication().endIgnoringInteractionEvents()
        })
    }
    
    // sets the current view, hiding the appropriate controls
    func setCurrentView(newCurrentView:CurrentViewType) {
        // switch between our potential UI components
        if (newCurrentView == CurrentViewType.Map) {
            // hide the table, show the map
            mapViewControl.hidden = false
            tableViewControl.hidden = true
            currentView = CurrentViewType.Map
        }
        else if (newCurrentView == CurrentViewType.Table) {
            // and vice versa ...
            mapViewControl.hidden = true
            tableViewControl.hidden = false
            currentView = CurrentViewType.Table
        }
    }
    
    // opens up the selected incident in the detail view
    func openIncidentDetail() {
        self.performSegueWithIdentifier("openIncidentSegue", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "openIncidentSegue" {
            var viewController:IncidentDetailController = segue.destinationViewController as! IncidentDetailController
            viewController.incident = selectedIncident!
        }
    }

    
    // MARK: - UITableViewDelegate
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedIncident = incidentList[indexPath.row]
        openIncidentDetail()
    }

    
    
    // MARK: - UITableViewDataSource
    
    // only the single section in our design
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    // likewise, just return the number of incidents that we pulled from SFDC
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return incidentList.count
    }
    
    // return the cell with the title that corresponds to the position in the incidents list
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableViewControl.dequeueReusableCellWithIdentifier("identifier", forIndexPath: indexPath) as! UITableViewCell
        var currentIncident:Incident = incidentList[indexPath.item]
        cell.textLabel?.text = currentIncident.subject
        return cell
    }
    
    // MARK: - MKMapViewDelegate
    func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView) {
        if view.annotation.isKindOfClass(Incident) {
            selectedIncident = (view.annotation as! Incident)
            openIncidentDetail()
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: SFRestDelegate methods
    
    func request(request: SFRestRequest?, didLoadResponse jsonResponse: AnyObject) {

        // setup
        var records = jsonResponse.objectForKey("records") as! NSArray
        
        var incidentsFromSFDC:[Incident] = [Incident]()
        
        // map the record from the json to the incident format
        for record in records {
            var incident:Incident = Incident()
            incident.id = record.objectForKey("Id") as! String
            incident.caseNumber = record.objectForKey("CaseNumber") as? String
            incident.accountName = record.objectForKey("Account")?.objectForKey("Name") as! String
            incident.status = record.objectForKey("Status") as? String
            incident.subject = record.objectForKey("Subject") as? String
            incident.incidentDescription = record.objectForKey("Description") as? String
            if let lastModifiedDateObject:AnyObject = record.objectForKey("LastModifiedDate") {
                incident.lastModified = SFDateUtil.SOQLDateTimeStringToDate(lastModifiedDateObject as! String)
            }
            if let closedDateObject:AnyObject = record.objectForKey("ClosedDate") {
                if (!(closedDateObject is NSNull)) {
                    incident.closedDate = SFDateUtil.SOQLDateTimeStringToDate(closedDateObject as! String)!
                }
            }
            if let latitudeObject:AnyObject = record.objectForKey("location__Latitude__s")  {
                if (!(latitudeObject is NSNull)) {
                    incident.latitude = (latitudeObject as! NSNumber).doubleValue
                }
            }
            if let longitudeObject:AnyObject = record.objectForKey("location__Longitude__s") {
                if (!(longitudeObject is NSNull)) {
                    incident.longitude = (longitudeObject as! NSNumber).doubleValue
                }
            }
            incidentsFromSFDC.append(incident)
        }
        updateIncidentsCallback(incidentsFromSFDC)
    }
    
    func request(request: SFRestRequest?, didFailLoadWithError error: NSError?) {
        showErrorFromSFDCLoad(error!.description)
    }
    
    func requestDidCancelLoad(request: SFRestRequest!) {
        showErrorFromSFDCLoad("The request was cancelled")
    }
    
    
    func requestDidTimeout(request: SFRestRequest!) {
        showErrorFromSFDCLoad("The request timed out, please try again")
    }
    
    func showErrorFromSFDCLoad(errorMessage:String) {
        dispatch_async(dispatch_get_main_queue(), {
            UIApplication.sharedApplication().endIgnoringInteractionEvents()
            self.activityIndicator.stopAnimating()
            var alert : UIAlertView = UIAlertView(title: "Error", message: errorMessage, delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        })
    }
    
}
