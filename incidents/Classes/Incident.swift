//
//  Incident.swift
//  incident_mobile
//
//  Data transfer object representing an individual incident
//
//  Created by Tim Hopkins on 2015-07-14.
//  Copyright (c) 2015 Deloitte Canada. All rights reserved.
//

import Foundation
import MapKit

class Incident: NSObject, MKAnnotation {
    
    var id:String
    var subject:String?
    var incidentDescription:String?
    var accountName:String
    var status:String?
    var latitude:Double {
        didSet {
            coordinate = CLLocationCoordinate2DMake(latitude, longitude)
        }
    }
    var longitude:Double {
        didSet {
            coordinate = CLLocationCoordinate2DMake(latitude, longitude)
        }
    }
    var lastModified:NSDate
    var caseNumber:String?
    var closedDate:NSDate?
    var coordinate:CLLocationCoordinate2D = CLLocationCoordinate2DMake(0,0)
    
    override init() {
        id = ""
        accountName = ""
        latitude = 0
        longitude = 0
        lastModified = NSDate()
    }
    
}
